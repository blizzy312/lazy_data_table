## [0.1.5]

* Fixed bug that caused headers to scroll separately.
* Expanded example.

## [0.1.4]

* Changed NotificationListener to GestureDetector.
* This allows for the ability to scroll in both directions at the same time.(diagonally)

## [0.1.3]

* Added DataTableDimensions, which holds information about the dimensions of the table.
* Added DataTableTheme, which holds information about the colors and borders of the table.
* Added example gif to the README.

## [0.1.2]

* Updated synchronized ScrollController to be more efficient.

## [0.1.1]

* Column and row headers are now optional.

## [0.0.1]

* Initial release
